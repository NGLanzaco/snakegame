/*
  ==============================================================================

    Waveguide1DWidget.h
    Created: 27 Jun 2019 9:31:09pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

class Waveguide1DWidget : Trackable
{
public:
    Waveguide1DWidget(//Property<float> &l,
                      //Property<float> &p,
                      //Property<float> &w
                      )
    {
//        l.BaseType::AddListener(this, [&] (float value) {this->SetLength(value);});
//        p.BaseType::AddListener(this, [&] (float value) {this->SetExcitePosition(value);});
//        w.BaseType::AddListener(this, [&] (float value) {this->SetWidth(value);});
        
//        l.BaseType::AddListener(this, [&] (float value) {this->Draw(shape);});
//        p.BaseType::AddListener(this, [&] (float value) {this->Draw(shape);});
//        w.BaseType::AddListener(this, [&] (float value) {this->Draw(shape);});
        
        Draw(shape);
    }
    
    void SetShape(const bool &shapeIndex) // make into an enum class
    {
        if (shapeIndex == 0) {
            shape = "triangle";
        } else if (shapeIndex == 1) {
            shape = "square";
        }
        
    }
    
    void SetLength(const int &l)
    {
        length = l;
//        Draw(shape);
    }
    
    void SetExcitePosition(const double &p)
    {
        position = p;
//        Draw(shape);
    }
    
    void SetWidth(const double &w)
    {
        width = w;
//        Draw(shape);
    }
    
    
    void SetExcitementCentre()
    {
//            if (position + (width / 2) >= 1) {
//                excitementCentre = length * position - (length * width/2);
//            } else {
//                excitementCentre = length * position + (length * width/2);
//            } else {
        excitementCentre = length * position;
    }
    
    void SetExcitementWidth()
    {
        excitementWidth = length * width;
    }
    
    void SetExcitementStart()
    {
        if (width == 1) {
            excitementStart = zeroX;
        }
        else {
            if (position + (width / 2.f) >= 1.f) {
                excitementStart = length - excitementWidth;
            } else if (position - (width / 2.f) <= 0.f) {
                excitementStart = zeroX;
            } else {
                excitementStart = excitementCentre - (excitementWidth / 2);
            }
        }
    }
    
    void SetExcitementEnd()
    {
        if (width == 1) {
            excitementEnd = length;
        } else {
            excitementEnd = excitementWidth + excitementStart;//excitementCentre + (excitementWidth / 2);
        }
    }
    
    
    void Draw(const std::string &shape)
    {
        if (shape == "triangle") {
            SetExcitementCentre();
            SetExcitementWidth();
            SetExcitementStart();
            SetExcitementEnd();
            
            l1.setStart(zeroX, zeroY);
            l1.setEnd(excitementStart.Get(), zeroY);
            
            l2.setStart(l1.getEnd());
            l2.setEnd(excitementCentre.Get(), zeroY - A);
            
            l3.setStart(l2.getEnd());
            l3.setEnd(excitementEnd.Get(), zeroY);
            
            l4.setStart(l3.getEnd());
            l4.setEnd(length.Get(), zeroY);
            
            arrayOfLines.add(l1);
            arrayOfLines.add(l2);
            arrayOfLines.add(l3);
            arrayOfLines.add(l4);
        } else if (shape == "square") {
            SetExcitementCentre();
            SetExcitementWidth();
            SetExcitementStart();
            SetExcitementEnd();
            
            l1.setStart(zeroX, zeroY);
            l1.setEnd(excitementStart.Get(), zeroY);
            
            l2.setStart(l1.getEnd());
            l2.setEnd(excitementStart.Get(), zeroY - A);
            
            l3.setStart(l2.getEnd());
            l3.setEnd(excitementEnd.Get(), zeroY - A);
            
            l4.setStart(l3.getEnd());
            l4.setEnd(excitementEnd.Get(), zeroY);
            
            l5.setStart(l4.getEnd());
            l5.setEnd(length.Get(), zeroY);
            
            arrayOfLines.add(l1);
            arrayOfLines.add(l2);
            arrayOfLines.add(l3);
            arrayOfLines.add(l4);
            arrayOfLines.add(l5);
        }
        
        
    }
    
    Array<Line<float>> Get()
    {
        return arrayOfLines;
    }
    
    //test public
    Property<float> length {400};
    Property<float> position {0.5};
    Property<float> width {0.1};
        
private:
    
    int A {100};
    int zeroY = 300; //starting point in the y axis
    int zeroX = 0;   //starting point in the x axis
    
    
    
    std::string shape {"triangle"};
//    std::string shape {"square"};
    
    
    Property<float> excitementCentre {0};// = length * position;
    Property<float> excitementWidth {0};// = length * width;
    Property<float> excitementStart {0};// = excitementCentre - (excitementWidth / 2);
    Property<float> excitementEnd {0};// = excitementCentre + (excitementWidth / 2);
    
    Array<Line<float>> arrayOfLines;
    Line<float> l1;
    Line<float> l2;
    Line<float> l3;
    Line<float> l4;
    
    Line<float> l5;
    
};
