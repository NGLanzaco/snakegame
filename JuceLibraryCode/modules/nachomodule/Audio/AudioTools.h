/*
  ==============================================================================

    AudioTools.h
    Created: 25 Jun 2019 18:50:40pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

inline double midiToFrequency(int midi, int fs = 44100) {
    //Do conversion
    return 0;
}

inline int freqToStringLength(const double &frequency, const int &fs = 44100) {
    int length = floor(fs/frequency);
    return length;
}
