/*
Copyright (C) Ignacio Gomez-Lanzaco, 2019 - All Rights Reserved
This module cannot be copied and/or distributed without the express permision of Ignacio Gomez-Lanzaco.
Written by Ignacio Gomez-Lanzaco <nacho.gomezlanzaco@gmail.com>, 2019
 */

/*******************************************************************************
 The block below describes the properties of this module, and is read by
 the Projucer to automatically generate project code that uses it.
 For details about the syntax and how to create or use a module, see the
 JUCE Module Format.txt file.

 #if 0
 BEGIN_JUCE_MODULE_DECLARATION

ID:                     nachomodule
vendor:                 Nacho Gomez-Lanzaco
name:                   mymodule
version:                1.0.0
description:            this is a Module
website:                https://nachogomezlanzaco.com
license:                MIT
minimumCppStandard:     17

dependencies:           juce_core, juce_data_structures, juce_events

 END_JUCE_MODULE_DECLARATION

#endif

*******************************************************************************/
#pragma once

#include <JuceHeader.h>
#include <math.h>

using namespace juce;

#include "Events/Blockable.h"
#include "Events/Trackable.h"
#include "Events/BaseSignal.h"
#include "Events/Property.h"
#include "Events/Chain.h"


//=============================================================================
/** Config: ENABLE_TESTS
Enable the Unit Tests
*/

#ifndef ENABLE_TESTS
#define ENABLE_TESTS 0
#endif

#if ENABLE_TESTS
#include "Events/Tests/Test.h"
#endif

//=============================================================================
/** Config: ENABLE_AUDIOTOOLS
Enable Audio Tools
*/

#ifndef ENABLE_AUDIOTOOLS
#define ENABLE_AUDIOTOOLS 0
#endif

#if ENABLE_AUDIOTOOLS
#include "Audio/AudioTools.h"
#endif


//=============================================================================
/** Config: ENABLE_WIDGETS
Enable Audio Tools
*/

#ifndef ENABLE_WIDGETS
#define ENABLE_WIDGETS 0
#endif

#if ENABLE_WIDGETS
#include "Widgets/Waveguide1DWidget.h"
#endif


//==
