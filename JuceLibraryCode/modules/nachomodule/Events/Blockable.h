/*
  ==============================================================================

    Blockable.h
    Created: 23 Jun 2019 12:02:53pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================

 Class definition... use?

*/


class Blockable
{
public:
    Blockable()
    : isBlocked(false)
    {

    }

    void SetBlocked(bool blocked) { isBlocked = blocked; }
    bool IsBlocked() const        { return isBlocked; }
private:
    bool isBlocked;
};
