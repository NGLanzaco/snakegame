/*
  ==============================================================================

    BaseSignal.h
    Created: 23 Jun 2019 12:02:20pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================

 BaseSignal is a templated signal that is trackable.
 It inherits from TRACKABLE and TRACKABLE::LISTENER in order to make the signal trackable.
 This means it will remove its listener automatically whenever it is deleted.

 In this base signal, the listeners are also Trackable - i.e. they are signals themselves, and they listen back to the signal.
 This way, the signal has a listener that listens to its changes, but the listener also has a listener, being the signal.
 Therefore, the listener listens to the signal changes and the signal listens to the listener changes.

*/


template <class Type>
class BaseSignal :  public Trackable,
public Trackable::Listener
{
public:
    ~BaseSignal()
    {
        for (auto &item : connections) {
            item->listener->RemoveListener(this);
        }
    }

    std::weak_ptr<Blockable> AddListener(Trackable *listener, std::function<void(Type)> callback)
    {
        std::shared_ptr<Connection> connection(new Connection (listener, callback));

        connections.emplace_back(connection);

        listener->AddListener(this);
        return connection;
    }

    void RemoveListener(Trackable *listener) {
        for (size_t i = 0; i < connections.size(); ++i)
        {
            auto &c = connections[i];

            if (c->listener == listener)
            {
                auto it = connections.begin();
                std::advance(it, i);
                connections.erase(it);

                listener->RemoveListener(this);
            }
        }
    }

protected:
    void NotifyListeners(Type value) {
        for (auto &connection : connections)
        {
            if (!connection->IsBlocked()) {
                connection->callback(value);
            }
        }
    }

private:

    void trackableWillBeDeleted(Trackable* object) override {
        RemoveListener(object);
    }

    class Connection
    : public Blockable
    {
    public:
        Connection(  Trackable                *listener
                   , std::function<void(Type)> callback)
        : listener(listener)
        , callback(callback)
        {

        }

        Trackable                *listener;
        std::function<void(Type)> callback;
    };

    std::vector<std::shared_ptr<Connection>> connections;
};
