/*
  ==============================================================================

    Chain.h
    Created: 23 Jun 2019 5:11:37pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

template <class SignalType>
inline void Chain(Property<SignalType> &dest, Property<SignalType> &src)
{
    dest.AddListener(src);
    dest = src;
    src.AddListener(dest);
}

template <class SignalTypeA, class SignalTypeB>
inline void Chain(Property<SignalTypeA> &dest, Property<SignalTypeB> &src)
{
    dest.BaseType::AddListener(&src, [&](SignalTypeB value){ src = static_cast<SignalTypeB>(value); });
    dest = src;
    src.BaseType::AddListener(&dest, [&](SignalTypeA value){ dest = static_cast<SignalTypeA>(value); });
}

template <class SignalType>
inline void UnChain(Property<SignalType> &dest, Property<SignalType> &src)
{
    dest.RemoveListener(&src);
    src.RemoveListener(&dest);
}

template <class SignalTypeA, class SignalTypeB>
inline void UnChain(Property<SignalTypeA> &dest, Property<SignalTypeB> &src)
{
    dest.RemoveListener(&src);
    src.RemoveListener(&dest);
}
