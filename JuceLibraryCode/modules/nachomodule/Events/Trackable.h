/*
  ==============================================================================

    Trackable.h
    Created: 23 Jun 2019 12:02:33pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================

 TRACKABLE class. A signal can inherit from TRACKABLE, to be a trackable signal - whenever it is deleted,
 it will automatically remove its listeners so that they stop listening to a 'non-existing- signal, hence avoiding crashes.
 It contains a 'tracker' or TRACKABLE::LISTENER.
 TRACKABLE::LISTENER has a pure virtual method that allows it to delete itself when its TRACKABLE is deleted

 Therefore, when a signal is TRACKABLE, it means that its got a tracker, and so, when it is deleted,
 the tracker will know and stop listening to the signal.

*/

class Trackable
{
protected:
    Trackable()
    {

    }

    ~Trackable()
    {
        listenerList.call(&Listener::trackableWillBeDeleted, this);
    }
public:
    class Listener //Tracker - Trackable::Listener
    {
    public:
        virtual void trackableWillBeDeleted(Trackable* object) = 0;
        virtual ~Listener() = default;
    };

    void AddListener(Listener* l)
    {
        listenerList.add(l);
    }
    void RemoveListener(Listener* l)
    {
        listenerList.remove(l);
    }

private:
    juce::ListenerList<Listener> listenerList;
};
