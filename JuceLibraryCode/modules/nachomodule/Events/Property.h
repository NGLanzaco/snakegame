/*
  ==============================================================================

    Property.h
    Created: 23 Jun 2019 12:03:09pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

template<class Type>
class Property : public BaseSignal<Type>
{
public:
    using SelfType = Property<Type>;
    using BaseType = BaseSignal<Type>;

    Property(Type initialValue) : propertyValue(initialValue)
    {

    }

    void Set(Type value) {
        if (propertyValue != value) {
            propertyValue = value;
            BaseSignal<Type>::NotifyListeners(propertyValue);
        }
    }
    Type const &Get() const {
        return propertyValue;
    }

    std::weak_ptr<Blockable> AddPropertyListener(SelfType &other)
    {
        return BaseType::AddListener(&other, [&] (Type value) {other = value;});
    }

    Property<Type>& operator= (Type newValue)
    {
        Set(newValue);
        return *this;
    }

    Property<Type>& operator= (const Property<Type> &other)
    {
        Set(other.Get());
        return *this;
    }

    template <class TypeB>
    Property<Type>& operator= (const Property<TypeB> &other)
    {
        Set(static_cast<TypeB>(other.Get()));
        return *this;
    }
    
//    Type& operator= (const Property<Type> &other)
//    {
//        Type a = other.Get();
//        return this = a;
//    }

    bool operator ==(const Property<Type> &other)
    {
        return Get() == other.Get();
    }
    
    template <class TypeB>
    bool operator ==(const Property<TypeB> &other)
    {
        return Get() == static_cast<Type>(other.Get());
    }

    bool operator ==(Type value)
    {
        return Get() == value;
    }

    bool operator !=(const Property<Type> &other)
    {
        return Get() != other.Get();
    }
    
    bool operator !=(Type value)
    {
        return Get() != value;
    }
    
    template <class TypeB>
    bool operator !=(const Property<TypeB> &other)
    {
        return Get() != static_cast<Type>(other.Get());
    }
    
    bool operator <=(const Property<Type> &other)
    {
        return Get() <= static_cast<Type>(other.Get());
    }
    
    bool operator >=(const Property<Type> &other)
    {
        return Get() >= static_cast<Type>(other.Get());
    }

    const Type& operator* ()
    {
        return *propertyValue;
    }

    const Type* operator-> ()
    {
        return &Get();
    }

    Property<Type>& operator/ (Type value)
    {
        Set(Get()/value);
        return *this;
    }
    
    Type& operator *(const Property<Type> &other) {
        Get() * other.Get();
        return *this;
    }
    
    Type& operator /(const Property<Type> &other) {
        Get() / other.Get();
        return *this;
    }
    
    Property<Type>& operator+(const Property<Type> &other) {
        Get() + other.Get();
        return *this;
    }
    
    Property<Type>& operator-(const Property<Type> &other) {
        Get() - other.Get();
        return *this;
    }
    
private:
    Type propertyValue;

};
