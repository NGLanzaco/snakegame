/*
  ==============================================================================

    Test.h
    Created: 23 Jun 2019 5:31:57pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/


class TestProperty : public UnitTest, public Trackable
{
public:
    TestProperty()
    : UnitTest("testProperty") {

    }
    
    void runTest() override
    {
        TestConstruction();
        TestAssignmentAndEqualityOp();
        TestPtrOp();
        TestBlockable();
        TestChain();
    }

    void TestConstruction() {
    //==========
        beginTest("propertyConstruction");
        Property<int> p(1);
        expect(p == 1);
    //==========
    }
    
    void TestAssignmentAndEqualityOp() {
    //==========
        beginTest("equalityOp");
        Property<int> p(1);
        Property<int> p2(2);
        p = p2;
        
        expect(p == p2);
        expect(p == 2);
        expect(p != 1);

        Property<float> p3(3.5);
        p = p3;
        expect(p == p3);
        expect(p == 3.5);
        expect(p3 != p2);
        expect(p2 != 5.5);
    }
    
    void TestPtrOp() {
        beginTest("testPointerOp");
        Property<std::string> pPtr = "test";
        
        expect(pPtr == nullptr);
    }
    
    void TestBlockable() {
        beginTest("connectionBlock");
        Property<int> p(1);
        bool hasCalledBack = false;
        std::weak_ptr<Blockable> blockableSignal;
        
        blockableSignal = p.BaseType::AddListener(this, [&] (int value) {hasCalledBack = true;});

        if (blockableSignal.expired() == false) {
            blockableSignal.lock()->SetBlocked(true);
        }
        
        p = 1;
        expect(hasCalledBack == false);
    //==========
        beginTest("connectionUnblock");
        if (blockableSignal.expired() == false) {
            blockableSignal.lock()->SetBlocked(false);
        }
        p = 2;
        expect(hasCalledBack == true);
    //==========
    }
    
    void TestChain() {
        beginTest("chain");
        Property<int> propertyA(10);
        Property<unsigned> propertyB(5);

        Chain(propertyA, propertyB);

        expect(propertyA == 5);
        propertyA = 100;

        expect(propertyB == 100);

        propertyB = 0;

        expect(propertyA == 0);
        UnChain(propertyA, propertyB);
    }
};
