/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "UI/GUIGridView.h"
#include "UI/GUIGridView.h"
#include "UI/GUIGridModel.h"
#include "UI/GUIGrid.h"
#include "Engine/View/ControlSurface.h"
#include "Adapters/GUIGridInterface.h"
#include "Adapters/GUIGridNotifier.h"
#include "Engine/Model/Snake.h"
#include "Engine/Controller/GameController.h"
#include "GridPoint.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    //==============================================================================
    // Your private member variables go here...
    ControlSurface controlSurface;
    
    GUIGridView gridView;
    GUIGridModel gridModel;
    GUIGrid grid {gridView, gridModel};
    
    GUIGridInterface gridInterface {grid, controlSurface};
    GUIGridNotifier gridNotifier {gridModel, controlSurface};
    
    Snake snake {4, 4};
    GridPoint fruit;
    
    GameController controller {controlSurface, snake, fruit};
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
