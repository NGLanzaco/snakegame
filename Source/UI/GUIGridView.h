/*
  ==============================================================================

    GUIGridView.h
    Created: 24 Nov 2019 8:37:01pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>

struct GUIGridView
{
    Array<std::shared_ptr<juce::TextButton>> pads;
    
    TextButton buttonUp;
    TextButton buttonDown;
    TextButton buttonLeft;
    TextButton buttonRight;
};
