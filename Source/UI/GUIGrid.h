/*
  ==============================================================================

    GUIGrid.h
    Created: 28 Oct 2019 8:18:40pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "UI/GUIGridModel.h"

//==============================================================================
/*
*/

struct GUIGridView;

class GUIGrid
: public Component
, public Button::Listener
{
public:
    GUIGrid(GUIGridView &view, GUIGridModel &model);
    ~GUIGrid();

    // juce::Component
    void paint (Graphics&) override;
    void resized() override;
    
    size_t GetNumCols();
    size_t GetNumRows();
    size_t GetNumPads();
        
    void SetPadColour(int index, Colour colour);
    void SetPadOff(int index);
    uint32_t GetPadColour(int index);
    Direction GetCurrentDirection();
    
    // juce::ButtonListener
    void buttonClicked(Button *button) override;
    
private:
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GUIGrid)
    
    GUIGridView &view;
    GUIGridModel &model;
};
