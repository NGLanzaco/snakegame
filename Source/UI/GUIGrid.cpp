/*
  ==============================================================================

    GUIGrid.cpp
    Created: 28 Oct 2019 8:18:40pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#include "UI/GUIGrid.h"
#include "UI/GUIGridView.h"
#include "UI/GUIGridModel.h"

//==============================================================================
GUIGrid::GUIGrid(GUIGridView &view, GUIGridModel &model)
: view(view)
, model(model)
{
    view.buttonUp.addListener(this);
    view.buttonDown.addListener(this);
    view.buttonLeft.addListener(this);
    view.buttonRight.addListener(this);
    
    for (size_t i = 0; i < model.numPads; i++)
    {
        model.pads.add(std::make_shared<Property<size_t>>(model.numPads + 1));
    }
    
    setSize(getLocalBounds().getWidth(), getLocalBounds().getHeight());
    
    for (size_t  i = 0; i < model.numPads; i++)
    {
        auto pad = std::make_shared<TextButton>();
        pad->setColour(TextButton::buttonColourId, model.padOffColour);
        pad->addListener(this);
        
        addAndMakeVisible(*pad);
        view.pads.add(pad);
    }
    
    view.buttonUp.setTriggeredOnMouseDown(true);
    view.buttonDown.setTriggeredOnMouseDown(true);
    view.buttonLeft.setTriggeredOnMouseDown(true);
    view.buttonRight.setTriggeredOnMouseDown(true);
    
    view.buttonUp.setColour(TextButton::buttonOnColourId, Colours::transparentBlack);
    view.buttonDown.setColour(TextButton::buttonOnColourId, Colours::transparentBlack);
    view.buttonLeft.setColour(TextButton::buttonOnColourId, Colours::transparentBlack);
    view.buttonRight.setColour(TextButton::buttonOnColourId, Colours::transparentBlack);
    
    addAndMakeVisible(view.buttonUp);
    addAndMakeVisible(view.buttonDown);
    addAndMakeVisible(view.buttonLeft);
    addAndMakeVisible(view.buttonRight);
}

GUIGrid::~GUIGrid()
{
}

void GUIGrid::paint (Graphics& g)
{
}

void GUIGrid::resized()
{
    auto width = getLocalBounds().getWidth() / model.numCols;
    auto height = getLocalBounds().getHeight() / model.numRows;
    
    for (size_t i = 0; i < model.numPads; i++)
    {
        size_t col = i % model.numCols;
        size_t row = i / model.numRows;
        
        auto x = col * width;
        auto y = row * height;
        
        auto pad = view.pads[int(i)];
        pad->setBounds(int(x), int(y), int(width), int(height));
    }
    
    int centreX = getLocalBounds().getWidth() / 2;
    int centreY = getLocalBounds().getHeight() / 2;
    view.buttonUp.setBounds(centreX - 10, centreY - 20, 20, 20);
    view.buttonDown.setBounds(centreX - 10, centreY, 20, 20);
    view.buttonLeft.setBounds(centreX - 30, centreY - 10, 20, 20);
    view.buttonRight.setBounds(centreX + 10, centreY - 10, 20, 20);
}

size_t GUIGrid::GetNumCols()
{
    return model.numCols;
}

size_t GUIGrid::GetNumRows()
{
    return model.numRows;
}

size_t GUIGrid::GetNumPads()
{
    return model.numPads;
}

void GUIGrid::SetPadColour(int index, Colour colour)
{
    view.pads[index]->setColour(TextButton::buttonColourId, colour);
}

void GUIGrid::SetPadOff(int index)
{
    view.pads[index]->setColour(TextButton::buttonColourId, model.padOffColour);
}

uint32_t GUIGrid::GetPadColour(int index)
{
    auto colour = view.pads[index]->findColour(TextButton::buttonColourId);
    return colour.getARGB();
}

Direction GUIGrid::GetCurrentDirection()
{
    return model.currentDirection.Get();
}

void GUIGrid::buttonClicked(Button *button)
{
    if (button == &view.buttonUp)
    {
        model.currentDirection = Direction::Up;
    }
    else if (button == &view.buttonDown)
    {
        model.currentDirection = Direction::Down;
    }
    else if (button == &view.buttonLeft)
    {
        model.currentDirection = Direction::Left;
    }
    else if (button == &view.buttonRight)
    {
        model.currentDirection = Direction::Right;
    }
    else
    {
        for (auto pad : view.pads)
        {
            if (button == &(*pad))
            {
                size_t padIndex = view.pads.indexOf(pad);
                model.pads[int(padIndex)]->Set(padIndex);
            }
        }
    }
}
