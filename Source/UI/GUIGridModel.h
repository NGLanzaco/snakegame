/*
  ==============================================================================

    GUIGridModel.h
    Created: 24 Nov 2019 8:58:23pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "Config.h"

//enum class Direction { Up, Down, Left, Right };

struct GUIGridModel
{
    const size_t  numCols {8};
    const size_t  numRows {8};
    const size_t  numPads = numRows * numCols;
    
    Colour padOffColour = Colours::grey;
    
    Property<Direction> currentDirection = Direction::Right;
    Array<std::shared_ptr<Property<size_t>>> pads;
};
