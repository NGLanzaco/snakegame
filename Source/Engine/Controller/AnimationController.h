/*
  ==============================================================================

    AnimationController.h
    Created: 18 Nov 2019 6:20:46pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include "GridPoint.h"

struct ControlSurface;

/**
    This is the Animation Controller that deals with the GameController's UI updates and animations.
 
    It performs changes to the User Interface in the form of Light Changes and Animations.
    As per MVC, this is the part of the controller that specifically updates the view.
 
 @see GameController, ControlSurface
 */

class AnimationController
{
public:
    /** Creates an AnimationController to update a ControlSurface.
        @param controlSurface  (view)
     */
    AnimationController(ControlSurface &controlSurface);
    
    /** Performs an Animation coverind the entire grid */
    bool GameOverAnimation();
    
    /** Displays a given score in the ControlSurface
        @param score    an integer score
     */
    void DisplayScore(const int &score);
    
    /** Clears the UI setting all ControlSurface pads off */
    void ClearUI();
    
    /** Resets the currentPadInAnimation to default. Used at the end of animations */
    void Reset();
    
    /** Sets a given pad to a given colour */
    void SetPadLight(GridPoint pad, uint32 colour);
    
    /** Sets a given pad colour off */
    void SetPadLightOff(GridPoint pad);
    
    /** Sets all pads off */
    void SetAllPadsOff();
    
    /** Returns the colour of a given pad
        @param pad      pad to get the colour of
     */
    const uint32_t GetPadColour(GridPoint pad);
    
private:
    
    /** Fills a given array with a set of points forming a number
        @param number       integer number to be displayed
        @param isInLeftHalf defines if the number should be displayed in the left half of the grid
        @param arrayToFill  array of Points to be filled
     */
    void GenerateNumberInGrid(const int &number, bool isInLeftHalf, GridPoint arrayToFill[]);
    
    ControlSurface &controlSurface;
    GridPoint currentPointInAnimation;
};
