/*
  ==============================================================================

    GameController.cpp
    Created: 16 Oct 2019 5:41:22pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#include "Engine/Controller/GameController.h"
#include "Engine/Model/Snake.h"
#include "GridPoint.h"
#include "Engine/Controller/AnimationController.h"
#include "Engine/View/ControlSurface.h"
#include <array>
#include <cmath>

GameController::GameController(ControlSurface &controlSurface, Snake &snake, GridPoint &fruit)
: controlSurface(controlSurface)
, snake(snake)
, fruit(fruit)
{
    controlSurface.direction.AddListener(this, [=] (Direction direction){this->OnButtonPressed(direction);});
   
    for (size_t i = 0; i < controlSurface.pads.size(); i++)
    {        controlSurface.pads[int(i)]->pressedState.AddListener(this, [=] (uint32_t index){this->OnPadPressed(i);});
    }
    
    GenerateNewFruit();
    Start();
}

void GameController::Start()
{
    startTimer(playingTimerID, timerInterval);
}

void GameController::Stop()
{
    stopTimer(restartTimerID);
}

void GameController::Restart(size_t padIndex)
{
    stopTimer(restartTimerID);
    ResetParameters(padIndex);
    Start();
}

void GameController::OnButtonPressed(Direction direction)
{
    if (!IsOpositeDirection(direction))
    {
        switch (direction) {
            case Direction::Up:
                snake.direction = Direction::Up;
                break;
                
            case Direction::Down:
                snake.direction = Direction::Down;
                break;
                
            case Direction::Left:
                snake.direction = Direction::Left;
                break;
                
            case Direction::Right:
                snake.direction = Direction::Right;
                break;
                
            default:
                break;
        }
    }
}

void GameController::OnPadPressed(size_t index)
{
    if (readyToRestart)
    {
        Restart(index);
    }
}

void GameController::timerCallback(int timerID)
{
    switch (timerID) {
        case playingTimerID:
            ProgressGame();
            break;
            
        case endGameTimerID:
            GameOver();
            break;
            
        case restartTimerID:
            PrepareToRestart();
            break;
            
        default:
            break;
    }
}

bool GameController::MoveSnake()
{
    GridPoint nextPoint = snake.body.getLast();
    switch (snake.direction) {
        case Direction::Up:
            if (nextPoint.y == 0)
            {
                nextPoint.y = (NUM_ROWS);
            }
            nextPoint.y = (nextPoint.y - 1);
            break;
            
        case Direction::Down:
            if (nextPoint.y == NUM_ROWS - 1)
            {
                nextPoint.y = 0;
            }
            else
            {
                nextPoint.y = nextPoint.y + 1;
            }
            break;
            
        case Direction::Left:
            if (nextPoint.x == 0)
            {
                nextPoint.x = NUM_COLS;
            }
            nextPoint.x = nextPoint.x - 1;
            break;
                
        case Direction::Right:
            if (nextPoint.x == NUM_COLS - 1)
            {
                nextPoint.x = 0;
            }
            else
            {
                nextPoint.x = nextPoint.x + 1;
            }
            break;
    }
    
    if (nextPoint == fruit)
    {
        Eat();
        IncreaseSnakeSpeed();
    }
    else if (snake.body.contains(nextPoint))
    {
        return true;
    }
    
    snake.body.add(nextPoint);
    snake.body.remove(0);
    return false;
}

void GameController::Eat()
{
    snake.body.add(fruit);
    score++;
    GenerateNewFruit();
}

void GameController::GenerateNewFruit()
{
    auto newRandomX = rand() % (NUM_ROWS);
    auto newRandomY = rand() % (NUM_COLS);
    GridPoint newFruitPoint(newRandomX, newRandomY);
    
    if (snake.body.contains(newFruitPoint))
    {
        GenerateNewFruit();
    }
    else
    {
        fruit = newFruitPoint;
    }
}

void GameController::IncreaseSnakeSpeed()
{
    startTimer(playingTimerID, timerInterval -= timerReduction);
    timerReduction = timerReduction / REDUCTION_RATIO;
}


void GameController::ProgressGame()
{
    bool snakeIsDead = MoveSnake();
    UpdateUserInterface();
    if (snakeIsDead)
    {
        stopTimer(playingTimerID);
        startTimer(endGameTimerID, 50);
    }
}

void GameController::GameOver()
{
    bool animationIsStillGoing = animationController.GameOverAnimation();
    
    if (!animationIsStillGoing)
    {
        stopTimer(endGameTimerID);
        startTimer(restartTimerID, 50);
    }
}

void GameController::PrepareToRestart()
{
    animationController.ClearUI();
    animationController.DisplayScore(score);
    readyToRestart = true;
}


void GameController::UpdateUserInterface()
{
    for (size_t i = 0; i < GRID_SIZE; i++)
    {
        size_t col = i % NUM_COLS;
        size_t row = i / NUM_ROWS;
        GridPoint currentPad(col, row);
        
        if (snake.body.contains(currentPad))
        {
            if (currentPad == snake.body.getLast())
            {
                animationController.SetPadLight(currentPad, SNAKE_HEAD_COLOUR);
            }
            else
            {
                animationController.SetPadLight(currentPad, SNAKE_BODY_COLOUR);
            }
        }
        else if (fruit == currentPad)
        {
            animationController.SetPadLight(currentPad, FRUIT_COLOUR);
        }
        else
        {
            animationController.SetPadLightOff(currentPad);
        }
    }
}

void GameController::ResetParameters(size_t padIndex)
{
    readyToRestart = false;
    score = 0;
    timerInterval = 800;
    timerReduction = 50;
    GenerateNewFruit();
    
    size_t newX = padIndex % NUM_COLS;
    size_t newY = padIndex / NUM_ROWS;
    GridPoint restartPad(newX, newY);
    snake.Reset(newX - 1, newY);
    
    controlSurface.Reset();
    animationController.Reset();
}

bool GameController::IsOpositeDirection(const Direction &direction)
{
    switch (direction) {
        case Direction::Up:
            if (snake.direction == Direction::Down)
            {
                return true;
            }
            else
            {
                return false;
            }
            break;
            
        case Direction::Down:
            if (snake.direction == Direction::Up)
            {
                return true;
            }
            else
            {
                return false;
            }
            break;
            
        case Direction::Left:
            if (snake.direction == Direction::Right)
            {
                return true;
            }
            else
            {
                return false;
            }
            break;
            
        case Direction::Right:
            if (snake.direction == Direction::Left)
            {
                return true;
            }
            else
            {
                return false;
            }
            break;
            
        default:
            break;
    }
}
