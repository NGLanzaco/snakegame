/*
  ==============================================================================

    AnimationController.cpp
    Created: 18 Nov 2019 6:20:46pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#include "Engine/Controller/AnimationController.h"
#include "ControllerHelpers.h"
#include "Engine/View/ControlSurface.h"
#include <array>


AnimationController::AnimationController(ControlSurface &controlSurface)
: controlSurface(controlSurface)
{
}

bool AnimationController::GameOverAnimation()
{
    GridPoint lastPad(NUM_COLS - 1, NUM_ROWS - 1);
    SetPadLight(currentPointInAnimation, GAME_OVER_COLOUR);
    
    if (currentPointInAnimation == lastPad)
    {
        return false;
    }
    else
    {
        if (currentPointInAnimation.x < NUM_COLS)
        {
            currentPointInAnimation.x += 1;
        }
        
        if (currentPointInAnimation.x == NUM_COLS && currentPointInAnimation.y < NUM_ROWS)
        {
            currentPointInAnimation.x = 0;
            currentPointInAnimation.y += 1;
        }
        return true;
    }
}

void AnimationController::DisplayScore(const int &score)
{
    GridPoint padsToColour[24];
    
    if (score < 10)
    {
        GenerateNumberInGrid(0, true, padsToColour);
        GenerateNumberInGrid(score, false, padsToColour);
    }
    else if (score >= 10 && score < 20)
    {
        GenerateNumberInGrid(1, true, padsToColour);
        GenerateNumberInGrid(score - 10, false, padsToColour);
    }
    
    for (const auto &pad : padsToColour)
    {
        SetPadLight(pad, SCORE_COLOUR);
    }
}

void AnimationController::ClearUI()
{
    SetAllPadsOff();
}

void AnimationController::Reset()
{
    ControllerHelpers::Reset(currentPointInAnimation);
}

void AnimationController::SetPadLight(GridPoint pad, uint32 colour)
{
    size_t padIndex = ControllerHelpers::GetIndexFromPad(pad);
    controlSurface.pads[int(padIndex)]->colour.Set(colour);
}

void AnimationController::SetPadLightOff(GridPoint pad)
{
    size_t padIndex = ControllerHelpers::GetIndexFromPad(pad);
    controlSurface.pads[int(padIndex)]->SetColourOff();
}

void AnimationController::SetAllPadsOff()
{
    for (auto &pad : controlSurface.pads)
    {
        pad->SetColourOff();
    }
}

const uint32_t AnimationController::GetPadColour(GridPoint pad)
{
    size_t padIndex = ControllerHelpers::GetIndexFromPad(pad);
    jassert(padIndex < GRID_SIZE);
    if (padIndex < GRID_SIZE)
    {
        return controlSurface.pads[int(padIndex)]->colour.Get();
    }
    else
    {
        return GAME_OVER_COLOUR;
    }
}

void AnimationController::GenerateNumberInGrid(const int &number, bool isInLeftHalf, GridPoint arrayToFill[])
{
//    if (isInLeftHalf)
//    {
//        if (number == 0)
//        {
//            arrayToFill[0].setXY(1, 2);
//            arrayToFill[1].setXY(2, 2);
//            arrayToFill[2].setXY(3, 2);
//            arrayToFill[3].setXY(1, 3);
//            arrayToFill[4].setXY(1, 4);
//            arrayToFill[5].setXY(1, 5);
//            arrayToFill[6].setXY(3, 3);
//            arrayToFill[7].setXY(3, 4);
//            arrayToFill[8].setXY(3, 5);
//            arrayToFill[9].setXY(1, 6);
//            arrayToFill[10].setXY(2, 6);
//            arrayToFill[11].setXY(3, 6);
//        }
//        else if (number == 1)
//        {
//            arrayToFill[0].setXY(1, 2);
//            arrayToFill[1].setXY(2, 2);
//            arrayToFill[2].setXY(2, 3);
//            arrayToFill[3].setXY(2, 4);
//            arrayToFill[4].setXY(2, 5);
//            arrayToFill[5].setXY(2, 6);
//            arrayToFill[6].setXY(1, 6);
//            arrayToFill[7].setXY(3, 6);
//            arrayToFill[8].setXY(1, 2); //
//            arrayToFill[9].setXY(1, 2); //
//            arrayToFill[10].setXY(1, 2); //
//            arrayToFill[11].setXY(1, 2); //
//        }
//        else if (number == 2)
//        {
//            arrayToFill[0].setXY(1, 2);
//            arrayToFill[1].setXY(2, 2);
//            arrayToFill[2].setXY(3, 2);
//            arrayToFill[3].setXY(3, 3);
//            arrayToFill[4].setXY(3, 4);
//            arrayToFill[5].setXY(1, 4);
//            arrayToFill[6].setXY(2, 4);
//            arrayToFill[7].setXY(1, 5);
//            arrayToFill[8].setXY(1, 6);
//            arrayToFill[9].setXY(1, 7);
//            arrayToFill[10].setXY(2, 7);
//            arrayToFill[11].setXY(3, 7);
//        }
//    }
//    else
//    {
//        if (number == 0)
//        {
//            arrayToFill[12].setXY(5, 2);
//            arrayToFill[13].setXY(6, 2);
//            arrayToFill[14].setXY(7, 2);
//            arrayToFill[15].setXY(5, 3);
//            arrayToFill[16].setXY(5, 4);
//            arrayToFill[17].setXY(5, 5);
//            arrayToFill[18].setXY(7, 3);
//            arrayToFill[19].setXY(7, 4);
//            arrayToFill[20].setXY(7, 5);
//            arrayToFill[21].setXY(5, 6);
//            arrayToFill[22].setXY(6, 6);
//            arrayToFill[23].setXY(7, 6);
//            
//        }
//        else if (number == 1)
//        {
//            arrayToFill[12].setXY(5, 2);
//            arrayToFill[13].setXY(6, 2);
//            arrayToFill[14].setXY(6, 3);
//            arrayToFill[15].setXY(6, 4);
//            arrayToFill[16].setXY(6, 5);
//            arrayToFill[17].setXY(6, 6);
//            arrayToFill[18].setXY(5, 6);
//            arrayToFill[19].setXY(7, 6);
//            arrayToFill[20].setXY(6, 5); //
//            arrayToFill[21].setXY(6, 6); //
//            arrayToFill[22].setXY(5, 6); //
//            arrayToFill[23].setXY(7, 6); //
//        }
//        else if (number == 2)
//        {
//            arrayToFill[12].setXY(5, 2);
//            arrayToFill[13].setXY(6, 2);
//            arrayToFill[14].setXY(7, 2);
//            arrayToFill[15].setXY(7, 3);
//            arrayToFill[16].setXY(7, 4);
//            arrayToFill[17].setXY(5, 4);
//            arrayToFill[18].setXY(6, 4);
//            arrayToFill[19].setXY(5, 5);
//            arrayToFill[20].setXY(5, 6);
//            arrayToFill[21].setXY(6, 6);
//            arrayToFill[22].setXY(7, 6);
//            arrayToFill[23].setXY(7, 6); //
//        }
//        else if (number == 3)
//        {
//            arrayToFill[12].setXY(5, 2);
//            arrayToFill[13].setXY(6, 2);
//            arrayToFill[14].setXY(7, 2);
//            arrayToFill[15].setXY(7, 3);
//            arrayToFill[16].setXY(7, 4);
//            arrayToFill[17].setXY(5, 4);
//            arrayToFill[18].setXY(6, 4);
//            arrayToFill[19].setXY(7, 5);
//            arrayToFill[20].setXY(7, 6);
//            arrayToFill[21].setXY(5, 6);
//            arrayToFill[22].setXY(6, 6);
//            arrayToFill[23].setXY(6, 4); //
//
//        }
//        else if (number == 4)
//        {
//            arrayToFill[12].setXY(5, 2);
//            arrayToFill[13].setXY(5, 3);
//            arrayToFill[14].setXY(5, 4);
//            arrayToFill[15].setXY(6, 4);
//            arrayToFill[16].setXY(7, 4);
//            arrayToFill[17].setXY(7, 3);
//            arrayToFill[18].setXY(7, 2);
//            arrayToFill[19].setXY(7, 5);
//            arrayToFill[20].setXY(7, 6);
//            arrayToFill[21].setXY(7, 4); //
//            arrayToFill[22].setXY(7, 3); //
//            arrayToFill[23].setXY(7, 5); //
//        }
//        else if (number == 5)
//        {
//            arrayToFill[12].setXY(7, 2);
//            arrayToFill[13].setXY(6, 2);
//            arrayToFill[14].setXY(5, 2);
//            arrayToFill[15].setXY(5, 3);
//            arrayToFill[16].setXY(5, 4);
//            arrayToFill[17].setXY(6, 4);
//            arrayToFill[18].setXY(7, 4);
//            arrayToFill[19].setXY(7, 5);
//            arrayToFill[20].setXY(7, 6);
//            arrayToFill[21].setXY(6, 6);
//            arrayToFill[22].setXY(5, 6);
//            arrayToFill[23].setXY(5, 6);
//        }
//        else if (number == 6)
//        {
//            arrayToFill[12].setXY(7, 2);
//            arrayToFill[13].setXY(6, 2);
//            arrayToFill[14].setXY(5, 2);
//            arrayToFill[15].setXY(5, 3);
//            arrayToFill[16].setXY(5, 4);
//            arrayToFill[17].setXY(5, 5);
//            arrayToFill[18].setXY(5, 6);
//            arrayToFill[19].setXY(6, 6);
//            arrayToFill[20].setXY(7, 6); //
//            arrayToFill[21].setXY(7, 5); //
//            arrayToFill[22].setXY(7, 4); //
//            arrayToFill[23].setXY(6, 4); //
//        }
//        else if (number == 7)
//        {
//            arrayToFill[12].setXY(5, 2);
//            arrayToFill[13].setXY(6, 2);
//            arrayToFill[14].setXY(7, 2);
//            arrayToFill[15].setXY(7, 3);
//            arrayToFill[16].setXY(7, 4);
//            arrayToFill[17].setXY(6, 4);
//            arrayToFill[18].setXY(7, 5);
//            arrayToFill[19].setXY(7, 6);
//            arrayToFill[20].setXY(5, 4);
//            arrayToFill[21].setXY(7, 4); //
//            arrayToFill[22].setXY(7, 5); //
//            arrayToFill[23].setXY(7, 6); //
//        }
//        else if (number == 8)
//        {
//            arrayToFill[12].setXY(6, 2);
//            arrayToFill[13].setXY(5, 3);
//            arrayToFill[14].setXY(7, 3);
//            arrayToFill[15].setXY(5, 4);
//            arrayToFill[16].setXY(6, 4);
//            arrayToFill[17].setXY(7, 4);
//            arrayToFill[18].setXY(5, 5);
//            arrayToFill[19].setXY(7, 5);
//            arrayToFill[20].setXY(7, 6);
//            arrayToFill[21].setXY(6, 6);
//            arrayToFill[22].setXY(5, 6);
//            arrayToFill[23].setXY(5, 2); //
//        }
//        else if (number == 9)
//        {
//            arrayToFill[12].setXY(5, 2);
//            arrayToFill[13].setXY(6, 2);
//            arrayToFill[23].setXY(7, 2);
//            arrayToFill[14].setXY(5, 3);
//            arrayToFill[15].setXY(7, 3);
//            arrayToFill[16].setXY(5, 4);
//            arrayToFill[17].setXY(6, 4);
//            arrayToFill[18].setXY(7, 4);
//            arrayToFill[19].setXY(7, 5);
//            arrayToFill[20].setXY(7, 6);
//            arrayToFill[21].setXY(5, 6);
//            arrayToFill[22].setXY(6, 6);
//        }
////        if (isInLeftHalf)
////        {
////            for (size_t i = 0; i < 24; i++)
////            {
////                arrayToFill[int(i)].setX(arrayToFill[int(i)].getX() - 4);
////            }
////        }
//    }
}
