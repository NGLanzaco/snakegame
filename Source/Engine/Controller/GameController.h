/*
  ==============================================================================

    GameController.h
    Created: 16 Oct 2019 5:41:22pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "Engine/Controller/AnimationController.h"

enum class Direction;
struct Snake;
struct GridPoint;
struct ControlSurface;

/**
    This is the main Controller in the MVC pattern that runs the game.
 
    Inherits from juce:MultiTimer and uses the timer callback to progress the game.
    Inherits from Trackable to listen to changes in the View [ControlSurface]
 
    As per MVC, the GameController updates its model [Snake, GridPoint] when the ControlSurface sends changes. The Controller then sends updates accordingly to the User Interface via an AnimationController
 
    @see ControlSurface, Snake, GridPoint
*/

class GameController
: private juce::MultiTimer
, Trackable
{
public:
    
    /** Creates a GameController from a ControlSurface, and a Snake and GridPoint.
        @param controlSurface  (view)
        @param snake           (model)
        @param fruit           (model) a GridPoint
     */
    GameController(ControlSurface &controlSurface, Snake &snake, GridPoint &fruit);
    
    /** Starts the Game */
    void Start();
    
    /** Stops the Game */
    void Stop();
    
    /** Re-Starts the Game from a specific index in the grid
        @param padIndex     index in the grid where the game snake will start
     */
    void Restart(size_t padIndex);
    
    /** Updates the model when Direction Buttons are pressed
        @param direction    DirectionButton pressed
     */
    void OnButtonPressed(Direction direction);
    
    /** Triggers a restart from a pad index when a pad is pressed, if the game is in readyToRestart state
        @param index    the index of the pressed pad
     */
    void OnPadPressed(size_t index);
    
private:
    /** Timer Callback function from juce::MultiTimer */
    void timerCallback (int timerID);
    
    /** Move the Snake by One step in the current Direction */
    bool MoveSnake();
    
    /** Append an additional pad at the end of the Snake body array.
     */
    void Eat();
    
    /** Generate a new GridPoint object at a random non-populated position
     */
    void GenerateNewFruit();
    
    /** Decreases the playingTimer callback time in order to make the Snake move faster
     */
    void IncreaseSnakeSpeed();
   
    /** Advance the game and trigger UpdateUserInterface() */
    void ProgressGame();
    
    /** End the game and trigger the GameOverAnimation */
    void GameOver();
    
    /** Trigger ClearUI and DisplayScore */
    void PrepareToRestart();
    
    /** Reflect changes in the User Interface. This is called by ProgressGame at every callback */
    void UpdateUserInterface();
    
    /** Reset all parameters in the view and the model back to default
        @param padIndex     index of pad where the snake will restart
     */
    void ResetParameters(size_t padIndex);
    
    bool IsOpositeDirection(const Direction &direction);
    
    
    enum timerID
    {
        playingTimerID = 0,
        endGameTimerID,
        restartTimerID
    };
    
    int timerInterval = 800;
    int score = 0;
    float timerReduction = 50;
    
    bool readyToRestart = false;
    
    ControlSurface &controlSurface;
    Snake &snake;
    GridPoint &fruit;
    AnimationController animationController {controlSurface};
};
