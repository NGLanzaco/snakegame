/*
  ==============================================================================

    Pad.cpp
    Created: 17 Dec 2019 8:57:46pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#include "Engine/View/Pad.h"

Pad::Pad(const size_t &x, const size_t &y)
: GridPoint(x, y)
{
}

void Pad::SetColourOff()
{
    colour.Set(OFF_COLOUR);
}

void Pad::Press()
{
    pressedState = true;
}

bool Pad::isPressed()
{
    return pressedState.Get();
}
