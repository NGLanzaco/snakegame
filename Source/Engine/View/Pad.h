/*
  ==============================================================================

    Pad.h
    Created: 17 Dec 2019 8:57:46pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include "Config.h"
#include "GridPoint.h"

/**
 A Pad is a GridPoint that holds a colour and a boolean pressed state. The ControlSurface uses and Array of these to form a grid.
 
    Both colour and pressedState are Properties, and therefore can be listened to.
    The GridInterface will listen to changes to colour (in order to reflect them to the right UI.
    The GameController will listen to changes to pressedState as part of the restart process.
 
    @see GridInterface, GameController, ControlSurface
 */

class Pad
: public GridPoint
{
public:
    /** Creates a Pad object */
    Pad(const size_t &x, const size_t &y);
    
    /** Sets the Pad colour to the Pad's colourOff */
    void SetColourOff();
    
    /** Sets the Pad's pressedState to true */
    void Press();
    
    /** Returns the current pressedState */
    bool isPressed();
    
    Property<uint32_t> colour {OFF_COLOUR};
    Property<bool> pressedState = false;
};
