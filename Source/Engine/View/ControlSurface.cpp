/*
  ==============================================================================

    ControlSurface.cpp
    Created: 21 Nov 2019 8:42:53pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#include "Engine/View/ControlSurface.h"
#include "ControllerHelpers.h"

ControlSurface::ControlSurface()
{
    for (size_t i = 0 ; i < GRID_SIZE; i++)
    {
        auto pad = std::make_shared<Pad>(i % NUM_COLS, i / NUM_ROWS);
        pads.at(i) = pad;
    }
}

void ControlSurface::Reset()
{
    for (const auto &pad : pads)
    {
        ControllerHelpers::Reset(*pad);
    }
    
    direction = Direction::Right;
}
