/*
  ==============================================================================

    ControlSurface.h
    Created: 21 Nov 2019 8:42:53pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once
#include <JuceHeader.h>
#include <array>
#include "Engine/View/Pad.h"
#include "Config.h"

/**
    The ControlSurface represents the View of the MVC running the application. It holds an Array of Pad objects used to form a grid, and a Direction property representing the currently selected direction
 
    As per MVC, the view [ControlSurface] is updated by the Controller [GameController] when the model [Snake, GridPoint] changes. The ControlSurface pads will then form the visual representation of the snake and the fruit and will update accordinly. Changes to the direction property in ControlSurface will dictate how the snake is moved.
 
    @see GameController, Pad, GridPoint, Snake
 */

struct ControlSurface
{
    /** Creates a ControlSurface object. It contains properties that can be listened to */
    ControlSurface();
    
    std::array<std::shared_ptr<Pad>, GRID_SIZE> pads;
    Property<Direction> direction = Direction::Right;
    
    /** Resets all its values to default*/
    void Reset();
};
