/*
  ==============================================================================

    Snake.cpp
    Created: 9 Oct 2019 6:30:08pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#include "Engine/Model/Snake.h"

Snake::Snake(size_t x, size_t y)
{
    GridPoint initialPoint(x, y);
    body.add(initialPoint);
    
    direction = Direction::Right;
}

void Snake::Reset(size_t newX, size_t newY)
{
    body.clear();
    GridPoint initialPoint(newX, newY);
    body.add(initialPoint);
    direction = Direction::Right;
}
