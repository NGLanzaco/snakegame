/*
  ==============================================================================

    Snake.h
    Created: 9 Oct 2019 6:30:08pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "GridPoint.h"
#include "Config.h"

//enum class SnakeDirection { Up, Down, Left, Right };

/**
    This is a snake object, which holds a body of GridPoint 's and colours for said body.
    It is part of the model in the MVC pattern running the game.
 
    As per MVC, the model [Snake, GridPoint]  is updated by the Controller [GameController] when the view [ControlSurface] changes. Changes to the Snake are then represented in the view as a response. The Snake moves in the ControlSurface grid as defined by the view parameters
 
    @see GameController,  GridPoint, ControlSurface
 */

struct Snake
{
    /** Creates a Snake object. A snake holds a body of Points and colours for said body, as well as a direction */
    Snake(size_t x, size_t y);
    
    Array<GridPoint> body;
    
    Direction direction;
    uint32_t colour = Colours::green.getARGB();
    uint32_t headColour = Colours::darkgreen.getARGB();
    
    /** Resets the Snake values and decreases its body down to a single Point. Set's that Point to a given x and y coordinates
        @param newX     new x position for the initial Point
        @param newY     new y position for the initial Point
     */
    void Reset(size_t newX, size_t newY);
};
