/*
  ==============================================================================

    Config.h
    Created: 18 Dec 2019 10:03:57pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#define NUM_COLS            8
#define NUM_ROWS            8
#define GRID_SIZE           NUM_COLS * NUM_ROWS

#define SNAKE_HEAD_COLOUR   0xFF006400
#define SNAKE_BODY_COLOUR   0xFF008000
#define FRUIT_COLOUR        0xFFFF0000
#define OFF_COLOUR          0xFF808080
#define SCORE_COLOUR        0xFF0000FF
#define GAME_OVER_COLOUR    0xFF000000

#define REDUCTION_RATIO     1.07

enum class Direction { Up, Down, Left, Right };
