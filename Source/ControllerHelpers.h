/*
  ==============================================================================

    ControllerHelpers.h
    Created: 18 Dec 2019 10:36:09pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once
#include "Engine/View/ControlSurface.h"
#include "Engine/Model/Snake.h"

namespace ControllerHelpers
{
    inline void Reset(Pad &pad)
    {
        pad.SetColourOff();
        pad.pressedState = false;
    }
    
    inline void Reset(size_t newX, size_t newY)
    {
        
    }
    
    inline void Reset(GridPoint &point)
    {
        point.x = 0;
        point.y = 0;
    }
    
    inline int GetIndexFromPad(const GridPoint &pad)
    {
        return int((pad.y * NUM_ROWS) + pad.x);
    }
}

