/*
  ==============================================================================

    GUIGridNotifier.h
    Created: 17 Dec 2019 10:24:14pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "Adapters/GridNotifier.h"
#include "UI/GUIGrid.h"

struct ControlSurface;
struct GUIGridModel;

/**
    The GUIGridNotifier is an implementation of the abstract class GridNotifier. It is used to send information from the  GUIGrid to the internal MVC's view
 
    It listens to the changes in the GUIGridModel and updates the ControlSurface accordinly.
 
 @see GridNotifier, GUIGridModel, ControlSurface
 */

class GUIGridNotifier
: public GridNotifier
, public Trackable
{
public:
    /** Creates a GUIGridNotifier from a GUIGridModel and a ControlSurface
     @param gridModel       a GUIGridModel that the Notifier will listen to for data changes
     @param controlSurface  a ControlSurface that the Notifier will send data changes to
     */
    GUIGridNotifier(GUIGridModel &gridModel, ControlSurface &controlSurface);
    
    /** Callback for changes to the GUIGridModel's direction
        @param direction        a given Direction that the Notifier will listen to
     */
    void OnDirectionChanged(Direction &direction);
    
    /** Callback for presses to the GUIGridModel's pads
        @param padIndex        a given pad's index in the GUIGridModel that the Notifier will listen to
     */
    void OnPadPressed(size_t &padIndex);
    
    /** The GUIGridNotifier implementation of the GridNotifier's function
        @see GridNotifier
     */
    void SetDirectionButton(const Direction &newDirection) override;
    
    /** The GUIGridNotifier implementation of the GridNotifier's function
        @see GridNotifier
     */
    void SetPadPressed(Pad &pad) override;
    
private:
    GUIGridModel &gridModel;
    ControlSurface &controlSurface;
};
