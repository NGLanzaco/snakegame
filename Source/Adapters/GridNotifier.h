/*
  ==============================================================================

    GridNotifier.h
    Created: 26 Nov 2019 6:57:43pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

enum class Direction;
class Pad;

/**
    The GridNotifier is an abstract class used to send information from a given UI to the internal MVC's view
 
    It listens to the changes in the UI's model and updates the ControlSurface [view] accordingly.
    Inheriting from this class will allow the creation of Notifier to adapt any UI to the game.
 
    The same way the GridInterface sends data from the internal engine to a givne UI, this GridNotifier responds to changes in any Grid UI and informs the internal ControlSurface of these changes
 
    @see GridInterface, ControlSurface
 */

class GridNotifier
{
public:
    /** Default destructor */
    virtual ~GridNotifier() = default;
    
    /** Sets the ControlSurface's direction following a given DirectionButton value
        @param newDirection     a DirectionButton chosen via the UI
     */
    virtual void SetDirectionButton(const Direction &newDirection) = 0;
    
    /** Sets the ControlSurface's padss pressedState following a given pad press in the UI
     @param pad     a Pad to be updated following the UI presses
     */
    virtual void SetPadPressed(Pad &pad) = 0;
};
