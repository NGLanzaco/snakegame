/*
  ==============================================================================

    GUIGridAdapter.cpp
    Created: 24 Nov 2019 10:35:37pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#include "Adapters/GUIGridInterface.h"
#include "UI/GUIGrid.h"
#include "Engine/View/ControlSurface.h"
#include "ControllerHelpers.h"

GUIGridInterface::GUIGridInterface(GUIGrid &grid, ControlSurface &controlSurface)
: grid(grid)
, controlSurface(controlSurface)
{
    
    for (auto pad : controlSurface.pads)
    {
        pad->colour.AddListener(this, [=] (uint32_t colourValue) {this->SetPadLight(*pad, colourValue);});
    }
}

GridPoint GUIGridInterface::GetLastPad()
{
    return lastPad;
}

void GUIGridInterface::SetPadLight(const GridPoint &pad, uint32 rgbColour)
{
    size_t padIndex = ControllerHelpers::GetIndexFromPad(pad);
    auto colour = juce::Colour(rgbColour);
        
    grid.SetPadColour(int(padIndex), colour);
}

void GUIGridInterface::SetPadLightOff(const GridPoint &pad)
{
    size_t padIndex = ControllerHelpers::GetIndexFromPad(pad);
    grid.SetPadOff(int(padIndex));
}

void GUIGridInterface::SetAllPadsOff()
{
    for (size_t i = 0; i < GRID_SIZE; i++) {
        grid.SetPadOff(int(i));
    }
}

uint32_t GUIGridInterface::GetPadColour(const GridPoint &pad)
{
    size_t padIndex = ControllerHelpers::GetIndexFromPad(pad);
    return grid.GetPadColour(int(padIndex));
}

void GUIGridInterface::SetDirectionLight(const Direction &button)
{
    
}

