/*
  ==============================================================================

    GUIGridAdapter.h
    Created: 24 Nov 2019 10:35:37pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "Adapters/GridInterface.h"

class GUIGrid;
struct ControlSurface;

/**
    The GUIGridInterface is an implementation of the abstract class GridInterface. It is used to send information from the MVC's view to the GUIGrid
 
    It listens to the changes in the ControlSurface and updates the UI accordinly.
 
    @see GridInterface, ControlSurface
 */

class GUIGridInterface
: public GridInterface
, public Trackable
{
public:
    /** Creates a GUIGridInterface from a GUIGrid and a ControlSurface
        @param grid             a GUIGrid that will receive data changes from the interface
        @param controlSurface   a ControlSurface that the interface will listen to for data changes
     */
    GUIGridInterface(GUIGrid &grid, ControlSurface &controlSurface);
    
    /** The GUIGridInterface implementation of the GridInterface's function
        @see GridInterface
     */
    GridPoint GetLastPad() override;
    
    /** The GUIGridInterface implementation of the GridInterface's function
     @see GridInterface
     */
    void SetPadLight(const GridPoint &pad, uint32 rgbColour) override;
    
    /** The GUIGridInterface implementation of the GridInterface's function
     @see GridInterface
     */
    void SetPadLightOff(const GridPoint &pad) override;
    
    /** The GUIGridInterface implementation of the GridInterface's function
     @see GridInterface
     */
    void SetAllPadsOff() override;
    
    /** The GUIGridInterface implementation of the GridInterface's function
     @see GridInterface
     */
    uint32_t GetPadColour(const GridPoint &pad) override;
    
    /** The GUIGridInterface implementation of the GridInterface's function
     @see GridInterface
     */
    void SetDirectionLight(const Direction &button) override;
    
private:
    GUIGrid &grid;
    ControlSurface &controlSurface;
    
    GridPoint lastPad {NUM_COLS, NUM_ROWS};
};
