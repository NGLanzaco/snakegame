/*
  ==============================================================================

    GUIGridNotifier.cpp
    Created: 17 Dec 2019 10:24:14pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#include "GUIGridNotifier.h"
#include "Engine/View/ControlSurface.h"

GUIGridNotifier::GUIGridNotifier(GUIGridModel &gridModel, ControlSurface &controlSurface)
: gridModel(gridModel)
, controlSurface(controlSurface)
{
    gridModel.currentDirection.AddListener(this, [=] (Direction direction){this->OnDirectionChanged(direction);});
    
    for (size_t i = 0; i < gridModel.pads.size(); i++)
    {
        gridModel.pads[int(i)]->AddListener(this, [&] (size_t padIndex){this->OnPadPressed(padIndex);});
    }
}

void GUIGridNotifier::OnDirectionChanged(Direction &direction)
{
    SetDirectionButton(direction);
}

void GUIGridNotifier::OnPadPressed(size_t &padIndex)
{
    jassert(padIndex < GRID_SIZE);
    if (padIndex < GRID_SIZE)
    {
        SetPadPressed(*controlSurface.pads[int(padIndex)]);
    }
}

void GUIGridNotifier::SetDirectionButton(const Direction &newDirection)
{
    controlSurface.direction = newDirection;
}

void GUIGridNotifier::SetPadPressed(Pad &pad)
{
    pad.Press();
}
