/*
  ==============================================================================

    Grid.h
    Created: 9 Oct 2019 6:30:16pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "Config.h"
#include "GridPoint.h"

/**
    The GridInterface is an abstract class used to send information from the MVC's view to whatever UI is desired
 
    It listens to the changes in the ControlSurface and updates the UI accordinly.
    Inheriting from this class will allow the creation of Interfaces to adapt any UI to the game.
 
    @see GameController, ControlSurface
 */

class GridInterface
{
public:
    
    /** Default destructor */
    virtual ~GridInterface() = default;
    
    /** Returns the Grid Size in GridPoint */
    virtual GridPoint GetLastPad() = 0;
    
    /** Sets a given pad's to a given colour
        @param pad          a given GridPoint Pad
        @param rgbColour    a given uint32 colour in rgb
     */
    virtual void SetPadLight(const GridPoint &pad, uint32 rgbColour) = 0;
    
    /** Sets a given pad's colour to its offColour
        @param pad          a given GridPoint Pad
     */
    virtual void SetPadLightOff(const GridPoint &pad) = 0;
    
    /** Sets all pads' colour to their offColour */
    virtual void SetAllPadsOff() = 0;
    
    /** Returns a given pad's colour
        @param pad          a given GridPoint Pad
     */
    virtual uint32_t GetPadColour(const GridPoint &pad) = 0;
    
    /** Sets a given DirectionButton's light
        @param button          a chosen DirectionButton
     */
    virtual void SetDirectionLight(const Direction &direction) = 0;
};
