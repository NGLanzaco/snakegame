/*
  ==============================================================================

    GridItem.h
    Created: 22 Oct 2019 7:36:11pm
    Author:  Nacho Gomez-Lanzaco

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

struct GridPoint
{
    GridPoint(const size_t &initX, const size_t &initY)
    : x(initX)
    , y(initY)
    {
    }
    
    GridPoint()
    : x(0)
    , y(0)
    {
    }
    
    size_t x;
    size_t y;
    
    bool operator ==(const GridPoint &other) const
    {
        return x == other.x && y == other.y;
    }
};
