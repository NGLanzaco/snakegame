var indexSectionsWithContent =
{
  0: "abcdghilmnoprsuv~",
  1: "acgmps",
  2: "acgmps",
  3: "abcdgimoprs~",
  4: "bcdhimnpv",
  5: "p",
  6: "ds",
  7: "dlru"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator"
};

